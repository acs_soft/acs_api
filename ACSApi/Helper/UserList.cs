﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ACSApi.Helper
{
    public class UserList
    {
        [DataMember]
        public string USER_ID { get; set; }
        [DataMember]
        public string NAME { get; set; }
        [DataMember]
        public string EMAIL { get; set; }
        [DataMember]
        public string MOBILE { get; set; }
        [DataMember]
        public string STATE { get; set; }
        [DataMember]
        public string CITY { get; set; }
        [DataMember]
        public string ROLE_ID { get; set; }
        [DataMember]
        public string STATUS { get; set; }
        [DataMember]
        public string DELETED { get; set; }

    }
}