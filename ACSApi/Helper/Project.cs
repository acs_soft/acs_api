﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ACSApi.Helper
{

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Project
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string send_status { get; set; }
        [DataMember]
        public string sid { get; set; }
        [DataMember]
        public string cid { get; set; }
        [DataMember]
        public string resid { get; set; }
        [DataMember]
        public string cname { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string In_verification { get; set; }
        [DataMember]
        public string Completed { get; set; }
        [DataMember]
        public string Awaiting { get; set; }
        [DataMember]
        public string Total { get; set; }
        [DataMember]

        public string email { get; set; }
        [DataMember]
        public string mobile { get; set; }
        [DataMember]
        public string address { get; set; }
        [DataMember]
        public string contact_person { get; set; }
        [DataMember]
        public string status { get; set; } 
        [DataMember]
        public string site_status { get; set; }
        [DataMember]
        public string deleted { get; set; }
        [DataMember]
        public string created_on { get; set; }
        [DataMember]
        public string created_by { get; set; }

        [DataMember]
        public string lastupdated_by { get; set; }

        [DataMember]
        public string lastupdated_on { get; set; }

        [DataMember]
        public string number_of_recipe { get; set; }


    }
}