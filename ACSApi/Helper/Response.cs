﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ACSApi.Helper
{
    [DataContract]
    public class Response
    {
        [DataMember]
        public string CODE { get; set; }
        [DataMember]
        public string MESSAGE { get; set; }
    }
}