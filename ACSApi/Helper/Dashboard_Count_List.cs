﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace ACSApi.Helper
{
    [DataContract]
    public class Dashboard_Count_List
    {
        [DataMember]
        public List<Dashboard_Count> data { get; set; }
        [DataMember]
        public List<Response> response { get; set; }
    }

    [DataContract]
    public class Dashboard_Count
    {
        [DataMember]
        public string LEAD_COUNT { get; set; }
        [DataMember]
        public string USER_COUNT { get; set; }
        [DataMember]
        public string DIBS_COUNT { get; set; }
        [DataMember]
        public string Approved_COUNT { get; set; }




    }
}