﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace ACSApi.Helper
{
    public class LeadList
    {

        [DataMember]
        public string LEAD_ID { get; set; }
        [DataMember]
        public string new_auto_incremented_volumn { get; set; }
        [DataMember]
        public string PU_DATE { get; set; }
        [DataMember]
        public string LOS_NO { get; set; }
        [DataMember]
        public string CUST_NAME { get; set; }
        [DataMember]
        public string CONTACT_NO { get; set; }
        [DataMember]
        public string LOGIN_BANK { get; set; }
        [DataMember]
        public string LOGIN_AMT { get; set; }
        [DataMember]
        public string DISB_AMT { get; set; }
        [DataMember]
        public string COMPANY_NAME { get; set; }
        [DataMember]
        public string CAT { get; set; }
        [DataMember]
        public string BT_FRESH { get; set; }
        [DataMember]
        public string PRODUCT { get; set; }
        [DataMember]
        public string LOCATION { get; set; }
        [DataMember]
        public string STATUS { get; set; }
        [DataMember]
        public string COMMENT { get; set; }
        [DataMember]
        public string CONNECTOR { get; set; }
        [DataMember]
        public string TL_NAME { get; set; }
        [DataMember]
        public string TSO_NAME { get; set; }
        [DataMember]
        public string LOGIN_DATE { get; set; }
        [DataMember]
        public string DISB_DATE { get; set; }
        [DataMember]
        public string DISB_MONTH { get; set; }
        [DataMember]
        public string CREATED_ON { get; set; }
        [DataMember]
        public string CREATED_BY { get; set; }
        [DataMember]
        public string LASTUPDATED_ON { get; set; }
        [DataMember]
        public string LASTUPDATED_BY { get; set; }
        [DataMember]
        public string DELETED { get; set; }
    }
}