﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ACSApi.Helper
{

    [DataContract]
    public class Disqualified_Director_List
    {
        [DataMember]
        public List<Disqualified_Director> data { get; set; }
        [DataMember]
        public List<Response> response { get; set; }
    }

    [DataContract]
    public class Disqualified_Director
    {
        [DataMember]
        public string DISQUALIFIED_DIRECTOR_ID { get; set; }
        [DataMember]
        public string DIN { get; set; }
        [DataMember]
        public string DIRECTOR_NAME { get; set; }
        [DataMember]
        public string COMPANY_NAME { get; set; }
        [DataMember]
        public string CIN { get; set; }
        [DataMember]
        public string ROC_LOCATION { get; set; }
        [DataMember]
        public string COMPANY_STATUS { get; set; }
        [DataMember]
        public string STATUS { get; set; }
        [DataMember]
        public string PERIOD_FROM { get; set; }
        [DataMember]
        public string PERIOD_TILL { get; set; }
    }
}