﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ACSApi.Helper
{

    [DataContract]
    public class Politically_exposed_List
    {
        [DataMember]
        public List<Politically_exposed> data { get; set; }
        [DataMember]
        public List<Response> response { get; set; }
    }

    [DataContract]
    public class Politically_exposed
    {
        [DataMember]
        public string POLITICALLY_EXPOSED_ID { get; set; }
        [DataMember]
        public string CANDIDATE_NAME { get; set; }
        [DataMember]
        public string CONSTITUENCY { get; set; }
        [DataMember]
        public string PARTY { get; set; }
        [DataMember]
        public string CRIMINAL_CASE { get; set; }
        [DataMember]
        public string EDUCATION { get; set; }
        [DataMember]
        public string TOTAL_ASSETS { get; set; }
        [DataMember]
        public string LIABILITIES { get; set; }
      
    }
}
