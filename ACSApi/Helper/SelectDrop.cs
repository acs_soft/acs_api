﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace ACSApi.Helper
{
    public class SelectDrop
    {
        [DataMember] public string id { get; set; }
        [DataMember] public string text { get; set; }
    }
}