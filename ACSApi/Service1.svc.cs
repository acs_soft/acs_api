﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Web.Configuration;
using ACSApi.Helper;


namespace ACSApi
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public Disqualified_Director_List disqualified_director_universal_search(string search_value)
        {
            Response response = new Response();
            Disqualified_Director_List data = new Disqualified_Director_List();
            List<Response> responses = new List<Response>();
            List<Disqualified_Director> resultlist = new List<Disqualified_Director>();
            Disqualified_Director Disqualified_Director = new Disqualified_Director();

           string str = ConfigurationManager.ConnectionStrings["ConnectionString1"].ToString();
            try
            {

                using (SqlConnection con = new SqlConnection(str))
                {

                    using (SqlCommand cmd = new SqlCommand("USP_DISQUALIFIED_DIRECTOR_MANAGMENT"))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@TYPE", "MASTER_SEARCH_DISQUALIFIED_DIRECTOR");
                        //cmd.Parameters.AddWithValue("@search_type", search_type);
                        cmd.Parameters.AddWithValue("@DIN", search_value);
                        con.Open();
                        DataSet ds = new DataSet();
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            sda.Fill(ds);
                        }
                        con.Close();
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                Disqualified_Director = new Disqualified_Director();
                                Disqualified_Director.DIN = ds.Tables[0].Rows[i]["DIN"].ToString();
                                Disqualified_Director.DIRECTOR_NAME = ds.Tables[0].Rows[i]["DIRECTOR_NAME"].ToString();
                                Disqualified_Director.COMPANY_NAME = ds.Tables[0].Rows[i]["COMPANY_NAME"].ToString();
                                Disqualified_Director.CIN = ds.Tables[0].Rows[i]["CIN"].ToString();
                                Disqualified_Director.ROC_LOCATION = ds.Tables[0].Rows[i]["ROC_LOCATION"].ToString();
                                Disqualified_Director.COMPANY_STATUS = ds.Tables[0].Rows[i]["COMPANY_STATUS"].ToString();
                                Disqualified_Director.STATUS = ds.Tables[0].Rows[i]["STATUS"].ToString();
                                Disqualified_Director.PERIOD_FROM = ds.Tables[0].Rows[i]["PERIOD_FROM"].ToString();
                                Disqualified_Director.PERIOD_TILL = ds.Tables[0].Rows[i]["PERIOD_TILL"].ToString();
                                resultlist.Add(Disqualified_Director);
                            }
                            data.data = resultlist;
                            response.CODE = WebConfigurationManager.AppSettings["success"];
                            response.MESSAGE = "Data available";
                            responses.Add(response);
                            data.response = responses;

                        }
                        else
                        {
                            response.CODE = WebConfigurationManager.AppSettings["success"];
                            response.MESSAGE = "Data not available";
                            responses.Add(response);
                            data.response = responses;

                        }



                    }


                }
            }


            catch (Exception e)
            {

                response.CODE = WebConfigurationManager.AppSettings["error"];
                response.MESSAGE = e.ToString();
                responses.Add(response);
                data.response = responses;


            }

            return data;



        }


        public Politically_exposed_List politically_exposed_universal_search(string search_value)
        {
            Response response = new Response();
            Politically_exposed_List data = new Politically_exposed_List();
            List<Response> responses = new List<Response>();
            List<Politically_exposed> resultlist = new List<Politically_exposed>();
            Politically_exposed Politically_exposed = new Politically_exposed();

            string str = ConfigurationManager.ConnectionStrings["ConnectionString1"].ToString();
            try
            {

                using (SqlConnection con = new SqlConnection(str))
                {

                    using (SqlCommand cmd = new SqlCommand("USP_POLITICALLY_EXPOSED_MANAGMENT"))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@TYPE", "MASTER_SEARCH_POLITICALLY_EXPOSED");
                        //cmd.Parameters.AddWithValue("@search_type", search_type);
                        cmd.Parameters.AddWithValue("@SEARCH_VALUE", search_value);
                        con.Open();
                        DataSet ds = new DataSet();
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            sda.Fill(ds);
                        }
                        con.Close();
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                Politically_exposed = new Politically_exposed();
                                Politically_exposed.CANDIDATE_NAME = ds.Tables[0].Rows[i]["CANDIDATE_NAME"].ToString();
                                Politically_exposed.CONSTITUENCY = ds.Tables[0].Rows[i]["CONSTITUENCY"].ToString();
                                Politically_exposed.PARTY = ds.Tables[0].Rows[i]["PARTY"].ToString();
                                Politically_exposed.CRIMINAL_CASE = ds.Tables[0].Rows[i]["CRIMINAL_CASE"].ToString();
                                Politically_exposed.EDUCATION = ds.Tables[0].Rows[i]["EDUCATION"].ToString();
                                Politically_exposed.TOTAL_ASSETS = ds.Tables[0].Rows[i]["TOTAL_ASSETS"].ToString();
                                Politically_exposed.LIABILITIES = ds.Tables[0].Rows[i]["LIABILITIES"].ToString();
                                resultlist.Add(Politically_exposed);
                            }
                            data.data = resultlist;
                            response.CODE = WebConfigurationManager.AppSettings["success"];
                            response.MESSAGE = "Data available";
                            responses.Add(response);
                            data.response = responses;

                        }
                        else
                        {
                            response.CODE = WebConfigurationManager.AppSettings["success"];
                            response.MESSAGE = "Data not available";
                            responses.Add(response);
                            data.response = responses;

                        }



                    }


                }
            }


            catch (Exception e)
            {

                response.CODE = WebConfigurationManager.AppSettings["error"];
                response.MESSAGE = e.ToString();
                responses.Add(response);
                data.response = responses;


            }

            return data;



        }

    }
}
