﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using ACSApi.Helper;


namespace ACSApi
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/disqualified_director_universal_search/{search_value=all}")]
        Disqualified_Director_List disqualified_director_universal_search(string search_value);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/politically_exposed_universal_search/{search_value=all}")]
        Politically_exposed_List politically_exposed_universal_search(string search_value);

    }
}
